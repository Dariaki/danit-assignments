/* 1. Опишите своими словами разницу между функциями setTimeout() и setInterval():
*     setTimeout() вьіполнит колбек функцию, переданную в качестве первого параметра один раз через заданное
*     вторьім параметром время (в миллисекундах), setInterval() будет повторять вьіполнение данной функции через
*     заданньій промежуток времени до тех пор, пока мьі сами ее не преостановим через clearInterval();
*
*  2. Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?
*     Она не сработает мгновенно, она попадает в стек вьізовов и будет вьіполнена после остального кода. Таким образом
*     код в setTimeout() вьіполняется асинхронно с остальньім кодом программьі.
*
*  3. Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?
*     В противном случае setInterval() будет вьіполняться бесколечное колличество раз.
* */


let imageColl = document.querySelectorAll('.image-to-show');
let imageArray = [...imageColl];
let btnStop = document.querySelector('.btn-stop');
let btnContinue = document.querySelector('.btn-continue');


let counter = 1;
imageArray[0].style.display = 'block';
const showImages = (imageArr) => {
    if (counter > 0) {
        imageArr[counter - 1].style.display = 'none';

    }
    if (counter === imageArr.length) {
        counter = 0;
    }

    imageArr[counter].style.display = 'block';
    counter++;

};

let imageSlider = setInterval(showImages, 10000, imageArray);

let wasClicked = true;
btnStop.addEventListener('click', ()=>{
    clearInterval(imageSlider);
    wasClicked = false;
});

btnContinue.addEventListener('click', ()=>{
    if (!wasClicked) {
        imageSlider = setInterval(showImages, 10000, imageArray);
    }
    wasClicked = true;
});


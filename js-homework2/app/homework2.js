/* Описать своими словами в несколько строчек, зачем в программировании нужны циклы:
*
* Циклы нужны чтобы повторять определенное действие бесконечное или заданное колличество раз.
* В случае с while, к примеру, пока не будет удовлетворять условие. Определенный код в while будет
* повторяться до тех пор, пока пользователь не введет необходимые данные. А цикл for может перебирать елементы
* массива(по индексам) или же просто числа, при этом у нас есть возможность задавать границы(точка начала и конца)
* и способ перебора(подряд, через один елемент и т.д.).
*/


// First part
let userNumber = +prompt('Enter the number divisible by 5: ', '10');

while(!Number.isInteger(userNumber)) {
    userNumber = +prompt('Enter the number divisible by 5: ', '10');
}

if (userNumber >= 0 && userNumber < 5) {
    console.log('Sorry, no such numbers');
} else {
    for (let num = 0; num < userNumber; num++) {
        if (num % 5 === 0) {
            console.log(num);
        }
    }

}



// Second part
let minNumber = +prompt('Enter minimal number: ');
let maxNumber = +prompt('Enter maximal number: ');


for (minNumber; minNumber < maxNumber; minNumber++) {
    if(isPrime(minNumber)) {
        console.log(minNumber);
    }
}

function isPrime(number) {
    if (number === 1) {
        return false;
    } else if (number === 2 || number === 3 || number === 5) {
        return true;
    } else if (number % 2 === 0 || number % 3 === 0 || number % 5 === 0) {
        return false;
    }
    return true;

}




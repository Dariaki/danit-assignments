$(document).ready(function(){
    $("a[href^='#']").on('click', function (event) {
        event.preventDefault();

        let target = this.hash; // #attractions;
        let $target = $(target); // after making an object this became our section!

        $('html').animate({
                'scrollTop': $target.offset().top  // #attractions.offset().top - distance from <html> to tag with id.
            }, 900, 'swing', function() {
                window.location.hash = target;
            }
        );
    });

    $('.section-news').hide();
    $('.toggle-btn').on('click', ()=> {
        $('.section-news').slideToggle(900);
    });

    let btnUp = $('.btn-up');
    btnUp.hide();
    $(window).on('scroll', ()=>{
        if ($(window).scrollTop() > $(window).height()) {
            $('.btn-up').fadeIn(900);
        } else {
            $('.btn-up').fadeOut(900);
        }
    });

    btnUp.on('click', ()=> {
        $("html").animate({scrollTop: 0}, 900);
    })

});

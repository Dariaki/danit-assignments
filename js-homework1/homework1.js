/* I. Обьяснить своими словами разницу между обьявлением переменных через var, let и const:
Var - переменная, что видна в рамках всей ф-ции. Она может существовать и до момента своего оглашения, в таком случае
она будет равна undefined.
Let - переменная, что видна в рамках одного конкретного блока {...}. До момента оглашения ее не существуєт (error).
Const - переменная-константа. В отличие от var и const ее значение нельзя изменять. Обычно задается один раз в
начале программы/ф-ции, название пишется в верхнем регистре.

II. Почему объявлять переменную через var считается плохим тоном?:
  Переменная var является устаревшей, вместо нее теперь используются let или const.
*/

let name = prompt('Please enter your name:', 'John Doe');

while (isFinite(parseInt(name)) || name === null || name === '') {
    name = prompt('Please enter your name again:', 'John Doe');
}

let age = parseInt(prompt('Please enter your age:', '24'));

while (isNaN(age) || age === null || age === '') {
    age = prompt('Please enter your age again:', '24');
}

if (age < 18) {
    alert('You are not allowed to visit this website');
} else if (age <= 22) {
    let confirm_replay = confirm('Are you sure you want to continue?');
    if (confirm_replay === true) {
        alert( `Welcome, ${name}!`);
    } else {
        alert('You are not allowed to visit this website');
    }
} else {
    alert(`Welcome, ${name}!`);
}
// HTML
let list = document.querySelector('.tabs');
let listText = document.querySelector('.tabs-content');

let listArr = [...list.children];
let listTextArr = [...listText.children];


// HELPERS
const zip = (list1, list2) => list1.map((li, liText) => {

    let attr = list2[liText].innerText;
    li.setAttribute('data-text', `${attr}`);
    return li;
});

zip(listArr, listTextArr);

const getTextSection = () => {
    let textSection = document.createElement('div');
    textSection.className = 'text-section';
    document.body.append(textSection);
    return textSection;
};

let textContainer = getTextSection();

// EVENTS
listArr.map((li) => {
    li.addEventListener('click', (event) => {
        let liText = event.target.dataset.text;

        let [tabsTitle, active] = [...event.target.classList];

        if (active) {
            event.target.classList.remove('active');
            textContainer.innerText = '';
            return;
        }

        textContainer.innerText = liText;

        for (let li of listArr)   {
            li.classList.remove('active');
        }
        event.target.classList.add('active');

    })
});

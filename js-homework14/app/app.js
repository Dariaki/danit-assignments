let tabs = $('.tabs-title');
let tabsContent = $('.tabs-content > li');
let textContainer = $('<div/>', {
    "class": "text-section"
});

textContainer.insertAfter('.tabs');

let tabsContentArr = [];
tabsContent.each(function () {
    tabsContentArr.push($(this).text());
});


let index = 0;
tabs.each(function() {
    return $(this).data("data-text", `${tabsContentArr[index++]}`);  // return $(this).attr("data-text", tabsContentArr[index++]);
});

tabs.on('click', function() {
    tabs.not($(this)).removeClass("active");
    $(this).toggleClass("active");

    if($(this).hasClass("active")) {
        textContainer.text(`${$(this).data("data-text")}`); // $`{$(this).attr("data-text")}`
    } else {
        textContainer.text("");
    }

});
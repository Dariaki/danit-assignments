/* Опишите своими словами, как Вы понимаете, что такое обработчик событий:
*
* Обработчик собьітий - єто функция, которую мьі прикрепляем к елементу и которая обрабатьівает заданное на єтом
* єлементе собьітие. В качестве аргументе принимает обьект (event), содержащий информацию о произощедшем собьітии.
*
* */


// HTML
let userInput = document.querySelector('.text-field');
let warning = document.querySelector('.warning');
let error = document.querySelector('.error');
let priceVal = document.querySelector('.price-value');
let priceValueContainer = document.querySelector('.price-value-container');


//HELPERS
const showError = (elem) => {
    elem.style.visibility = 'visible';
};

const hideError = (elem) => {
    elem.style.visibility = 'hidden';
};

const removeElement = (elem) => {
    elem.remove();
};

//EVENTS
userInput.addEventListener('keypress', (event) => {

    if (event.which < 45 || event.which > 57) {
        showError(warning);
        setTimeout(hideError, 3000, warning);
        event.preventDefault();
    }
});

userInput.addEventListener('focus', () => {

    userInput.style.border = '3px solid #0e9c34';

});


userInput.addEventListener('change', () => {


    userInput.style.border = '3px solid #cccccc';

    if (userInput.value < 0) {
        userInput.style.borderColor = 'red';
        showError(error);
        setTimeout(hideError, 3000, error);
        userInput.value = '';
        return;
    }

    let priceValue = priceVal.cloneNode(true);
    priceValue.style.display = 'inline-block';
    let priceValueText = document.createElement('span');
    priceValueText.className = 'price-value-text';

    priceValueText.innerText = `Price: $${userInput.value}`;
    priceValue.append(priceValueText);
    priceValueContainer.append(priceValue);

    priceValue.childNodes[0].addEventListener('click', () => {
        removeElement(priceValue);
    });
    userInput.value = '';
});

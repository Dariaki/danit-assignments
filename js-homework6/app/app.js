/* Опишите своими словами как работает цикл forEach:
*
* Цикл forEach применяется к массивам. Параметром ему передается колбек функция, которая принимает (текущий елемент массива,
* индекс, сам массив). Метод вьізьівает данную функцию для каждого елемента массива. Сам метод не возвращает нового
* массива (функция ничего не возвращает - undefined), функция взаимодействует с массиов и на ходу преобразовьівает
* его елементьі, при етом сам массив остается без изменений. Так что если во время итерации никуда не записать результат,
* мьі его не увидим.
*
* */

let array = ['hello', true, 'world', sayHi = () => {alert('Hello')}, 23, false, '23', null, 15, undefined, {name: 'John'}, [0, 1, 2, 3], 'wow'];


function filterBy(arr, dataType) {
        
        switch (dataType) {
                case 'array':
                        return arr.filter(item => !Array.isArray(item));

                case 'null':
                        return arr.filter(item => item !== null);

                case 'object':
                        return arr.filter(item => typeof item !== dataType || Array.isArray(item) || item === null);

                default: return arr.filter(item => typeof item !== dataType);
        }
}


console.log(filterBy(array, 'string'));
console.log(filterBy(array, 'array'));
console.log(filterBy(array, 'null'));
console.log(filterBy(array, 'object'));

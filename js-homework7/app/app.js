/* Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM):
* DOM - обьектная модель документа. Иньіми словами весь наш HTML документ представляется в виде обьектов и свойств,
* где теги, текст и даже комментарии внутри них - обьектьі (узльі), у которьіх есть различньіе свойства, с которьіми мьі
* можем производить разньіе манипуляции и которьіе хранят в себе данньіе про етот обьект.
*
* */


const array1 = ['hello', 'world', {name: 'John', age: 19}, 'Kiev', 'Kharkiv', 'Odessa', ['1', '2', '3'], 'Lviv', [4, 5, 6]];
// const array2 = ['1', '2', '3', 'sea', 'user', 23];


const list = document.createElement('ul');
document.body.prepend(list);


const getList = arr => {

    arr.map(item => {
        if (Array.isArray(item)) {
            const li = document.createElement('li');
            const subList = document.createElement('ul');
            list.insertAdjacentElement('beforeend', li); //list.append(li);
            li.insertAdjacentElement('beforeend', subList);  //li.append(subList);
            item.map(subItem => subList.insertAdjacentHTML('beforeend', `<li>${subItem}</li>`))

        } else if (typeof item === 'object' && !Array.isArray(item)) {
            const li = document.createElement('li');
            const subList = document.createElement('ul');
            list.insertAdjacentElement('beforeend', li);
            li.insertAdjacentElement('beforeend', subList);
            for (let el in item) {
                subList.insertAdjacentHTML('beforeend', `<li>${el}: ${item[el]}</li>`);
            }
        } else {
            list.insertAdjacentHTML('beforeend', `<li>${item}</li>`);
        }
    });

};


const timerBlock = document.createElement('div');
document.body.prepend(timerBlock);
timerBlock.textContent = '10';
timerBlock.className = 'timer';
timerBlock.style.cssText = `
                    box-sizing: border-box;
                    width: 200px; 
                    height: 50px;
                    background-color: #90eefc;
                    border: 1px solid #90eefc;
                    border-radius: 6px;
                    font-size: 20px;
                    text-align: center;
                    padding-top: 12px`;


let seconds = document.getElementsByClassName('timer')[0].textContent;

const showTimer = setInterval(function() {
    seconds--;
    document.getElementsByClassName('timer')[0].textContent = seconds;
    if (seconds <= 0) clearInterval(showTimer);
}, 1000);


const clearPage = (...args) => {
    setTimeout(() => {
        args.map(arg => arg.remove());
    }, 10000);
};


getList(array1);
clearPage(list, timerBlock);





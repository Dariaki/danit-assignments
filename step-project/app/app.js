$(document).ready(function () {

    // Image Gallery based on Masonry
    const gallery = $('.section-images__images-gallery');
    const sizer = '.sizer';

    gallery.imagesLoaded(()=>{
        gallery.masonry({
            itemSelector: '.images-gallery__image-item',
            columnWidth: sizer,
            percentPosition: true,
            horizontalOrder: true
        });
    });

    $('.img-load-more-btn').on( 'click', function() {
        const randomImgNumber = Math.floor(Math.random() * 7);
        // create new item elements
        console.log(randomImgNumber);
        let $items = $(`<li class="images-gallery__image-item sizer image-item-hover imgs-load-more-session1 display-none-image-gallery">
                             <img src="./img/images-gallery/image${randomImgNumber + 1}.png" alt="hotel-image" class="image-item__img">
                             <div class="image-item__image-hover">
                                 <i class="fa fa-search"></i>
                                 <i class="fa fa-recycle"></i>
                             </div>
                       </li>`);
        // append items to grid
        $('.section-images__images-gallery').append( $items )
        // add and lay out newly appended items
            .masonry( 'appended', $items );
    });


    // Service tabs changing
    const navTabs = $('.services-list__item');
    const descriptionImgs = $('.services-description__img');
    const descritionTexts = $('.services-description__text');

    const descriptionImgsContent = [];
    descriptionImgs.each(function () {
        descriptionImgsContent.push($(this).attr('src'));
    });

    const descritionTextsContent = [];
    descritionTexts.each(function () {
        descritionTextsContent.push($(this).text());
    });

    let indexImg = 0;
    let indexTxt = 0;
    navTabs.each(function () {
        $(this).data("data-image", `${descriptionImgsContent[indexImg++]}`);
        $(this).data("data-text", `${descritionTextsContent[indexTxt++]}`);
    });

    navTabs.on('click', function() {
        navTabs.not($(this)).removeClass("activeTab");
        $(this).addClass("activeTab");

        if($(this).hasClass("activeTab")) {
            $('.service-text-container').text(`${$(this).data("data-text")}`);
            $('.service-img-container').attr('src', `${$(this).data("data-image")}`)
        }
    });


    // Working Gallery - Load More Button
    const workImgSession1 = $('.session1');
    const workImgSession2 = $('.session2');
    const workImgSession3 = $('.session3');
    const workImg = $('.works-gallery__item');
    workImg.addClass('selected-item');
    let clickedCounter = 0;


    const removeOurClass = (element, classToRemove) => {
        element.removeClass(classToRemove);
    };


    $('.works-gallery-btn').click(function () {
        switch (clickedCounter) {
            case 0:
                setTimeout(removeOurClass, 2000, workImgSession2, 'display-none-works-gallery');
                break;
            case 1:
                setTimeout(removeOurClass, 2000, workImgSession3, 'display-none-works-gallery');
                $('.works-gallery-btn').addClass('display-none-works-gallery');
                break;
        }
        clickedCounter++;
    });

    // Filters in Amazing Section

    const btnFilter = (btn, elements, selector) => {
        btn.hide();
        elements.show();
        elements.not(selector).hide();
    };

    $('.all-works-tab').click(function () {
        $('.works-gallery-btn').hide();
        $('.works-gallery__item').show();
    });

    $('.graphic-design-tab').click(function () {
        btnFilter($('.works-gallery-btn'), workImg, '.graphic-design-item');
    });

    $('.web-design-tab').click(function () {
        btnFilter($('.works-gallery-btn'), workImg, '.web-design-item');
    });

    $('.landing-page-tab').click(function () {
        btnFilter($('.works-gallery-btn'), workImg, '.landing-page-item');
    });

    $('.wordpress-tab').click(function () {
        btnFilter($('.works-gallery-btn'), workImg, '.wordpress-item');
    });


    // Slider
    $('.feedback-slider__single-slide').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.feedback-slider__navigation'
    });
    $('.feedback-slider__navigation').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.feedback-slider__single-slide',
        centerMode: true,
        focusOnSelect: true,
        prevArrow: '.prevBtn',
        nextArrow: '.nextBtn'
    });


});





/*1. Описать своими словами для чего вообще нужны функции в программировании:
*
* Функции нужны для того, чтобы быстро и удобно переиспользовать один и тот же участок кода много раз.
* Таким образом функции позволяют избегать повторений и способствовать оптимизации. Один раз написав какую-то
* функцию, например функцию сложения, мы сможем использовать эту ее в разных скриптах, не переписывая все заново.
* Чтобы с функциями было хорошо работалось, желательно делать их как можно более независимыми от окружения. Функция
* должна выполнять какое-то одно определенное действие (например сложение двух чисел). Для вычитания уже должна быть
* другая функция.
*
* 2. Описать своими словами, зачем в функцию передавать аргумент:
*
* Аргументы, это данные которые мы передаем в функцию извне (например пользовательский ввод). Функция их обрабатывает
* и на выходе мы получаем результат. Например, в функцию, которая переводит градусы из Фаренгейта в Цельсий, мы
* передадим числовой аргумент - градус по Фагенрейту, а функция произведет вычисления и вернет на выходе градус
* по Цельсию.
*
* */


let firstNumber = prompt('Enter first number: ');
let secondNumber = prompt('Enter second number: ');

while (isNaN(parseFloat(firstNumber)) || isNaN(parseFloat(secondNumber))) {
    firstNumber = prompt('Enter first number again: ', firstNumber);
    secondNumber = prompt('Enter second number again: ', secondNumber);
}

firstNumber = parseFloat(firstNumber);
secondNumber = parseFloat(secondNumber);

let operator = prompt('Enter operator (+ - * /): ');

function calculator(num1, num2, option) {
    if (option === '+') {
        return addition(num1, num2);
    } else if (option === '-') {
        return subtraction(num1, num2);
    } else if (option === '*') {
        return multiplication(num1, num2);
    } else if (option === '/') {
        return division(num1, num2);
    } else {
        return 'Incorrect operator';
    }
}

function addition(num1, num2) {
    return num1 + num2;
}

function subtraction(num1, num2) {
    return num1 - num2;
}

function multiplication(num1, num2) {
    return num1 * num2;
}

function division(num1, num2) {
    if (num2 !== 0) {
        return num1 / num2;
    }
    return 'Division by zero is impossible!';
}

console.log(calculator(firstNumber, secondNumber, operator));

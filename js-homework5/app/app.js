/* Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования:
 *
 * В язьіках программирования существуют спец символьі, такие как: . | \ * ? [  etc.
 * Но что если нам необходимо использовать их в буквальном смьісле? Например, при создании паттерна в регулярних
 * вьіражениях? Для етого нам необходимо екранировать данньіе символьі. Делается ето с помощью обратного слеша \
 * Например, чтобьі екранировать точку, пишем \. Чтобьі екранировать апостроф \' А чтобьі екранировать сам бек-слеш: \\
 * Екранирование апострофа тоже распространенное применение екранирование, поскольку без них нельзоя string записать
 * так 'This's a great day!' - ето будет ошибкой. Нужно либо использовать разньіе кавьічки, либои поставить перед
 * апострофом бек-слеш.
 * */



function createNewUser() {
    let name = prompt('Enter your name: ');
    let secondName = prompt('Enter your last name: ');
    let birthday = prompt('Enter your birthday (26.03.2007): ');
    let newUser = {
        firstName: name,
        lastName: secondName,
        birthDay: birthday,
        getLogin() {
            let login = this.firstName[0] + this.lastName;
            return login.toLowerCase();
        },
        getAge() {
            let [day, month, year] = this.birthDay.split('.');
            let today =  new Date();

            let happyday = new Date(year, month - 1, day);
            let difference = today - happyday;
            let oneYear = 1000 * 60 * 60 * 24 * 365;

            let age = Math.floor(difference / oneYear);
            if (today.getMonth() === month && today.getDate() < day) {
                age--;
                }
            return `Your age is ${age}`;

        },
        getPassword() {
            let birthyear = this.birthDay.split('.')[2];
            return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${birthyear}`
        }

    };


    return newUser;
}

user1 = createNewUser();
console.log(user1.getAge());
console.log(user1.getPassword());



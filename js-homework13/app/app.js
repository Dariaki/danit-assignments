const createLink = (startPoint) =>{
    let link = document.createElement('link');
    link.setAttribute("rel", "stylesheet");
    link.setAttribute("href", "./css/second-theme.css");
    link.setAttribute("id", "second-theme-file");
    startPoint.after(link);
    return link;
};

let toggleBtn = document.querySelector('.btn-color-change');
let firstTheme = document.querySelector('#first-theme-file');
let secondTheme = createLink(firstTheme);
secondTheme.remove();

let theme;

let isClicked = false;
const changeTheme = () => {
    toggleBtn.addEventListener('click', ()=>{

        if (!isClicked) {
            firstTheme.after(secondTheme);
            localStorage.setItem('theme', `secondTheme`);
            isClicked = true;
        } else {
            secondTheme.remove();
            localStorage.setItem('theme', `firstTheme`);
            isClicked = false;
        }
    });
};
changeTheme();

theme = localStorage.getItem('theme');
if (theme === 'secondTheme') {
    firstTheme.after(secondTheme);
    isClicked = true;
}

//HTML
let labels = document.querySelectorAll('.input-wrapper');
let passwordForm = document.querySelector('.password-form');

let labelsArr = [...labels];


// HELPERS
const createWarning = () => {
    let warning = document.createElement('div');
    warning.className = 'warning';
    warning.innerText = 'Passwords don\'t match!';
    return warning;
};

const removeWarning = (element) => {
    element.style.display = 'none';
};

const showWarning = (element) => {
    element.style.display = 'inline-block';
};

const invalidPassword = createWarning();
labelsArr[labelsArr.length - 1].after(invalidPassword);

//EVENTS
labelsArr.map((lable) => {
    let labelItemsArr = [...lable.children];
    let [input, icon] = labelItemsArr;

    let [fas, faToggle, iconPass] = [...icon.classList];

    if (faToggle === 'fa-eye') {
        input.setAttribute('type', 'text');
    }

    icon.addEventListener('click', (event) => {

        let [fas, faToggle, iconPass] = [...icon.classList];

        if (faToggle === 'fa-eye') {

            event.target.classList.replace('fa-eye', 'fa-eye-slash');
            input.setAttribute('type', 'password');
            return;
        }

        event.target.classList.replace('fa-eye-slash', 'fa-eye');
        input.setAttribute('type', 'text');

    });
});

passwordForm.addEventListener('submit', (event) => {

    if (event.target[0].value === event.target[1].value && (event.target[0].value && event.target[1].value)) {
        event.target[0].value = '';
        event.target[1].value = '';
        alert('You are welcome!');
        return;
    }
    event.preventDefault();
    event.target[0].value = '';
    event.target[1].value = '';
    showWarning(invalidPassword);
    setTimeout(removeWarning, 3000, invalidPassword);

});

let btnContainer = document.querySelector('.btn-wrapper');
let buttons = [...btnContainer.children];


document.addEventListener('keypress', (event) => {
    let key = event.key.toLowerCase();

    for (let button of buttons) {
        if (button.innerText.toLowerCase() === key) {
            let [btn, active] = [...button.classList];
            if (active) {
                button.classList.remove('btn-pressed');
                return;
            }
            for (let button of buttons) {
                button.classList.remove('btn-pressed');
            }
            button.classList.add('btn-pressed');
        }

    }
})